package it.epicode.be.es0804.th2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RunnableDog extends Dog implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(RunnableDog.class);

    private String message;

    public RunnableDog(String message) {
        logger.debug("Enter ctor");
        this.message = message;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            logger.info(message);
            if (i % 3 == 0) {
                bark();
            }
        }
    }
}
