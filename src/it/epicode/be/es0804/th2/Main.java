package it.epicode.be.es0804.th2;

public class Main {
    public static void main(String[] args) {
        Runnable r1 = new RunnableDog("ST 1");
        Runnable r2 = new RunnableDog("ST 2");

        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);
        t1.start();
        t2.start();

    }
}
