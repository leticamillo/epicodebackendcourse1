package it.epicode.be.es0804.th1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SampleThread extends Thread {
    private static final Logger logger = LoggerFactory.getLogger(SampleThread.class);

    public SampleThread(String message) {
        super(message);
        logger.debug("Enter ctor");
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            logger.info(getName());
        }
    }
}
